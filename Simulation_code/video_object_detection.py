import cv2
import numpy as np
from matplotlib import pyplot as plt
from scipy.ndimage import rotate



# Charger l'algorithme Yolo       
net = cv2.dnn.readNet("yolov3.weights", "yolov3.cfg")  
classes = []
with open("objets.names", "r") as f:
    classes = [line.strip() for line in f.readlines()]
layer_names = net.getLayerNames()
output_layers = [layer_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]
colors = np.random.uniform(0, 255, size=(len(classes), 3)) # genere des couleur aleatore qu'on va utiliser pour les rectangles

#**************** Cette partie est tous ce qu'on a besoin pour charger notre algorithme 



#dans cette partie on a esseye de lire une video ou bien utiliser la camera principal pour lire la video on direct

#video = cv2.VideoCapture('video_test.mp4')  #lire une video 
video = cv2.VideoCapture(0)  #lire la video a partir de la camera principal 
fps=video.get(cv2.CAP_PROP_FPS) #lire le nombre de photos par second de notre video
print("Frames per second :{0}".format(fps)) #affichier le nombre de photos par second 
cv2.namedWindow('PROJET',cv2.WINDOW_NORMAL) #renomer la fenetre
cv2.resizeWindow('PROJET',1400,1000) #redimenssioner la fenetre

while(True):
    hasFrames, frame=video.read() # lire la video frame par frame 
    frame = cv2.resize(frame, None, fx=0.4, fy=0.4)
    height, width, channels = frame.shape # extraire les dimenssion de chaque frame
  
    # cette partie est ajouté si on a parfois des images en sortie decaler de queleques degres, alors elle vient pour ajuster la sortie
   
    # (t, n)=frame.shape[:2]
   #center=(t/2, n/2)
   #angle=-90.0
   #scale=0.5
    # Perform the rotation
   #M=cv2.getRotationMatrix2D(center, angle, scale)
   #frame=cv2.warpAffine(frame, M, (t, n))
    #frame = rotate(frame, -90)


# partie de detection des objets sur un farme (image ) : 
    # avant que chaque frame passe l'algorithle yolo nous devons effectuer une operation 
    blob = cv2.dnn.blobFromImage(frame, 0.00392, (416, 416), (0, 0, 0), True, crop=False) #convertir l'image (frame) en un blob
    #le blob qui est un moyen d'extraire les fonctionnalités d'une image  
    
    #maintenat l'image est prête pour passer par l'algorithme
    net.setInput(blob)  # passer l'image comme entrer pour l'algorithme
    outs = net.forward(output_layers)  #recuperer la sortie de l'algorithme 

    # passer a la representation gratique des resultats  :
    class_ids = []
    confidences = []
    boxes = []
    for out in outs:
        for detection in out:
            scores = detection[5:]
            class_id = np.argmax(scores)
            confidence = scores[class_id]
            if confidence > 0.5: # tester sur le confiance de detention de l'objet a l'aide de l'algorithme 
                # apres la detection de l'objet on vas esseyer de trouver son centre pour qu'on puisse le cadrer 
                center_x = int(detection[0] * width)
                center_y = int(detection[1] * height)
                w = int(detection[2] * width)
                h = int(detection[3] * height)

                # trassage des rectangles sur les objets detecter
                x = int(center_x - w / 2)
                y = int(center_y - h / 2)
                
                #mettre les differentes parammetres ainsi que classes des objets d'un image dans des tableaux
                boxes.append([x, y, w, h]) #stocke les positions des objets
                confidences.append(float(confidence)) #stocke la confience du resultat obtenue a l'aide de l'algorithme
                class_ids.append(class_id)
    #cette fonction va nous permettre de suprimer les bruits au tour du rectangle 
    indexes = cv2.dnn.NMSBoxes(boxes, confidences, 0.5, 0.4)
    print(indexes) # affichage des indices des objets detecter
    font = cv2.FONT_HERSHEY_PLAIN
    # danscette partie on va esseyer d'ecrire au-dessus de chaque objet deteter ca classe 
    for i in range(len(boxes)):
        if i in indexes:
            x, y, w, h = boxes[i]
            label = str(classes[class_ids[i]])
            color = colors[class_ids[i]]
            cv2.rectangle(frame, (x, y), (x + w, y + h), color, 2)
            cv2.putText(frame, label, (x, y + 30), font, 3, color, 3)
            cv2.putText(frame, "Master Informatique   ", (10,10 ) , font, 1, (255, 0, 0), 1)
            cv2.putText(frame, "Et Telecommunication ", (10,20 ) , font, 1, (255, 0, 0), 1)
    cv2.imshow("PROJET", frame)
    if cv2.waitKey(1)&0xFF==ord('q'):
        break
    else:
        cv2.waitKey(10);
cv2.waitKey(0)
cv2.destroyAllWindows()
