\select@language {french}
\contentsline {chapter}{INTRODUCTION G\IeC {\'E}N\IeC {\'E}RALE}{3}{chapter*.2}
\contentsline {chapter}{\numberline {1}INTRODUCTION AU TRAITEMENT ET L'ANALYSE DE VID\IeC {\'E}O}{4}{chapter.1}
\contentsline {section}{\numberline {I.}Introduction}{4}{section.1.1}
\contentsline {section}{\numberline {II.}Vid\IeC {\'e}o num\IeC {\'e}rique et analogique}{4}{section.1.2}
\contentsline {subsection}{\numberline {1.}Vid\IeC {\'e}o Num\IeC {\'e}rique}{5}{subsection.1.2.1}
\contentsline {subsection}{\numberline {2.}Sous-\IeC {\'e}chantillonnage }{6}{subsection.1.2.2}
\contentsline {section}{\numberline {III.}Segmentation s\IeC {\'e}mantique}{6}{section.1.3}
\contentsline {section}{\numberline {IV.}Conclusion}{6}{section.1.4}
\contentsline {chapter}{\numberline {2}SUIVRE ET DETECTION D'OBJETS YOLO AVEC OPENCV}{7}{chapter.2}
\contentsline {section}{\numberline {I.}Introduction}{7}{section.2.1}
\contentsline {section}{\numberline {II.}Classification d'images}{8}{section.2.2}
\contentsline {section}{\numberline {III.}D\IeC {\'e}tection d'objet dans une image }{10}{section.2.3}
\contentsline {chapter}{\numberline {3}CODE ET SIMULATION}{15}{chapter.3}
\contentsline {chapter}{WEBOGRAPHIE}{18}{lstnumber.-1.99}
